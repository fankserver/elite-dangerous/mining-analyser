import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SelectorComponent} from './journal/selector/selector.component';
import {DetailComponent} from './journal/detail/detail.component';
import {StatisticComponent} from './statistic/statistic.component';
import {ExportComponent} from './export/export.component';
import {ImportComponent} from './import/import.component';
import {LogComponent} from './log/log.component';
import {LocationsComponent} from './locations/locations.component';
import {Id64Component} from "./id64/id64.component";

const routes: Routes = [
  { path: '', component: SelectorComponent, },
  { path: 'detail/:id', component: DetailComponent, },
  { path: 'statistic', component: StatisticComponent, },
  { path: 'export', component: ExportComponent, },
  { path: 'import', component: ImportComponent, },
  { path: 'locations', component: LocationsComponent, },
  { path: 'logs', component: LogComponent, },
  { path: 'id64', component: Id64Component, },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
