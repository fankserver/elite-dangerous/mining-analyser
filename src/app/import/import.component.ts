import {Component, Inject} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {JournalService} from '../journal.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent {
  public isImporting = false;

  constructor(
    private journal: JournalService,
    private http: HttpClient,
    @Inject(APP_BASE_HREF) private baseHref: string,
    private router: Router
  ) { }

  public importDemo(file: string, location: string) {
    this.isImporting = true;

    this.http.get(`${this.baseHref === '/' ? '' : this.baseHref}/assets/demo/${file}`, {
      responseType: 'text',
    }).subscribe(
      (content) => {
        this.journal.reset();

        for (const line of content.split('\n')) {
          if (line === '') {
            continue;
          }

          this.journal.newEvent(line);
        }

        this.journal.analyze();
        this.isImporting = false;

        if (location) {
          this.router.navigateByUrl(location);
        }
      }
    );
  }

  public groupJournal(event: any) {
    const input = event.target;

    this.journal.reset();

    let fileReadCounter = 0;
    for (const file of input.files) {
      const reader = new FileReader();
      reader.readAsText(file);
      reader.onload = () => {
        for (const line of (reader.result as string).split('\n')) {
          if (line === '') {
            continue;
          }

          this.journal.newEvent(line);
        }
      };

      reader.onloadend = () => {
        fileReadCounter++;

        if (input.files.length === fileReadCounter) {
          this.journal.analyze({
            ignoreStartAndEnd: true,
            translate: true,
          });
        }
      };
    }
  }
}
