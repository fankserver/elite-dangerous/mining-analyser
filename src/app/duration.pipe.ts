import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(duration: number): string {
    const hours = Math.floor(duration / 3600000);
    duration -= hours * 3600000;
    const minutes = Math.floor(duration / 60000);
    duration -= minutes * 60000;
    const seconds = Math.floor(duration / 1000);
    return `${hours}h ${('0' + minutes).slice(-2)}m ${('0' + seconds).slice(-2)}s`;
  }

}
