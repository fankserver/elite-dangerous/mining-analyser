import { Component, OnInit } from '@angular/core';
import {JournalService, MaterialPercentile, MiningRun} from '../../journal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {norms} from '../../norms';

export interface MaterialStat {
  material: string;
  highest?: number;
  average?: number;
  amount?: number;
}

@Component({
  selector: 'app-journal-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  public objectKeys = Object.keys;
  get cumulativeFrequencyChartType(): string {
    return localStorage.getItem('JournalDetail_CumulativeFrequencyChartType') || 'CDFb';
  }
  set cumulativeFrequencyChartType(value: string) {
    localStorage.setItem('JournalDetail_CumulativeFrequencyChartType', value);
  }

  public run$: Observable<MiningRun>;
  public cargoChartData: any[] = [];
  public asteroidMaterialCountChartData: any[] = [];
  public asteroidMaterialPercentilesCharData: any[] = [];
  public asteroidMaterialCumulativeFrequencyChartDataCDFb: {[key: string]: any[]} = {};
  public asteroidMaterialCumulativeFrequencyChartDataCDF: {[key: string]: any[]} = {};
  public asteroidMaterialStats: MaterialStat[] = [];
  public asteroidMaterialStatsColumns: any[] = ['material', 'amount', 'average', 'highest'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private journal: JournalService,
  ) { }

  ngOnInit() {
    this.run$ = this.journal.activeRun$.pipe(
      tap((run) => {
        this.cargoChartData = [];
        this.asteroidMaterialCountChartData = [];
        this.asteroidMaterialPercentilesCharData = [];
        this.asteroidMaterialStats = [];
        this.asteroidMaterialCumulativeFrequencyChartDataCDFb = {};
        this.asteroidMaterialCumulativeFrequencyChartDataCDF = {};

        for (const event of run.events) {
          let entryIndex = null;
          for (const entry in this.cargoChartData) {
            if (this.cargoChartData[entry].name === event.type) {
              entryIndex = entry;
              break;
            }
          }
          if (entryIndex == null) {
            this.cargoChartData.push({
              name: event.type,
              series: [],
            });
            entryIndex = this.cargoChartData.length - 1;
          }

          this.cargoChartData[entryIndex].series.push({
            name: new Date(event.timestamp),
            value: event.amount,
          });
        }

        for (const material in run.prospectedAsteroidsMaterialCount) {
          if (run.prospectedAsteroidsMaterialCount.hasOwnProperty(material)) {
            this.asteroidMaterialCountChartData.push({
              name: material,
              value: run.prospectedAsteroidsMaterialCount[material],
            });
          }
        }

        const commodityPercentile: {[key: string]: MaterialPercentile[]} = {};
        for (const entry of run.prospectedAsteroidMaterialPercentiles) {
          if (!commodityPercentile[entry.name]) {
            commodityPercentile[entry.name] = [];
          }
          commodityPercentile[entry.name].push(entry);
        }

        for (const commodity in commodityPercentile) {
          if (commodityPercentile.hasOwnProperty(commodity)) {
            const series: any[] = [];
            for (const entry of commodityPercentile[commodity]) {
              series.push({
                name: entry.percentile,
                y: commodity,
                x: entry.percentile,
                r: entry.amount,
              });
            }
            this.asteroidMaterialPercentilesCharData.push({
              name: commodity,
              series,
            });
          }
        }

        for (const commodity in commodityPercentile) {
          if (commodityPercentile.hasOwnProperty(commodity)) {
            this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity] = [];
            this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity] = [];

            for (const entry of commodityPercentile[commodity]) {
              // cumulative frequency CDFb
              let cumulatedAmountCDFb = 0;
              for (const innerEntry of commodityPercentile[commodity]) {
                if (innerEntry.percentile > entry.percentile) {
                  cumulatedAmountCDFb += innerEntry.amount;
                }
              }
              this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity].push({
                name: entry.percentile,
                value: cumulatedAmountCDFb / run.prospectedAsteroidCount,
              });

              // cumulative frequency CDF
              this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity].push({
                name: entry.percentile,
                value: 1 - (cumulatedAmountCDFb / run.prospectedAsteroidCount),
              });
            }

            // cumulative frequency CDFb
            this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity] = [
              {
                name: 'x',
                series: this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity],
              }
            ];
            if (norms[commodity]) {
              for (const norm in norms[commodity]) {
                if (norms[commodity].hasOwnProperty(norm)) {
                  this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity].push({
                    name: norm,
                    series: norms[commodity][norm],
                  });
                }
              }
            }

            // cumulative frequency CDF
            this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity] = [
              {
                name: 'x',
                series: this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity],
              }
            ];
            if (norms[commodity]) {
              for (const norm in norms[commodity]) {
                if (norms[commodity].hasOwnProperty(norm)) {
                  const series = JSON.parse(JSON.stringify(norms[commodity][norm]));
                  for (const entry of series) {
                    entry.value = 1 - entry.value;
                  }

                  this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity].push({
                    name: norm,
                    series,
                  });
                }
              }
            }
          }
        }

        for (const material in run.highestProspectedAsteroidsPercentile) {
          if (run.highestProspectedAsteroidsPercentile.hasOwnProperty(material)) {
            this.asteroidMaterialStats.push({
              material,
              highest: run.highestProspectedAsteroidsPercentile[material] / 100,
            });
          }
        }

        for (const material in run.prospectedAsteroidMaterialPercent) {
          if (run.prospectedAsteroidMaterialPercent.hasOwnProperty(material)) {
            for (const i in this.asteroidMaterialStats) {
              if (this.asteroidMaterialStats[i].material !== material) {
                continue;
              }
              // Fix for issue #11
              const netNumberOfProspectedAsteroids = run.prospectedAsteroidCount - run.prospectedAsteroidDuplicateCount;
              const total = run.prospectedAsteroidMaterialPercent[material].reduce((a, b) => a + b, 0);
              console.log(material, run.prospectedAsteroidCount, total, run.prospectedAsteroidMaterialPercent[material].length);
              this.asteroidMaterialStats[i].average = (total / netNumberOfProspectedAsteroids) / 100;
              this.asteroidMaterialStats[i].amount = run.prospectedAsteroidMaterialPercent[material].length;
            }
          }
        }

        this.asteroidMaterialStats.sort((a, b) => {
          if (a.material < b.material) { return -1; }
          if (a.material > b.material) { return 1; }
          return 0;
        });
      }),
    );

    this.route.paramMap.subscribe((params) => {
      if (!this.journal.selectRun(+params.get('id'))) {
        this.router.navigate(['/']);
      }
    });
  }

  public percentTickFormatting(val: any): string {
    return val.toLocaleString('en-uk', {style: 'percent', maximumSignificantDigits: 1});
  }
}
