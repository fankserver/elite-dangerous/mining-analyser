import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {HttpClient} from '@angular/common/http';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Observable, Subject} from 'rxjs';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {debounceTime, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class LocationsComponent implements OnInit {
  public filter: string;
  public dataSource: MatTableDataSource<any>;
  public basicColumns: string[] = ['systemDistance', 'bodyDistanceToStar', 'bodyName', 'commodity', 'reporter'];
  public detailedColumns: string[] = ['overlapID', 'systemDistance', 'bodyDistanceToStar', 'bodyName', 'bodyReserve', 'bodyRingTypes', 'commodity', 'reporter', 'createdAt', 'approved'];
  public showDetail = false;
  public displayedColumns: string[] = this.basicColumns;
  public expandedElement: any;
  public filteredSystems$: Observable<any[]>;
  public systemFilter$: Subject<string> = new Subject<string>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private http: HttpClient) {
    this.dataSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource.paginator.pageSize = 10;
    this.dataSource.sort.sort({
      id: 'systemDistance',
      start: 'asc',
      disableClear: false,
    });

    this.filteredSystems$ = this.systemFilter$.pipe(
      debounceTime(500),
      switchMap((systemName) => {
        return this.http.get<any[]>('https://edgalaxy.fankserver.com/systems/autocomplete/' + systemName);
      })
    );

    this.getOverlapsNearSystem('Sol');
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  searchSystems(systemName: string) {
    this.systemFilter$.next(systemName);
  }

  getOverlapsNearSystem(systemName: string) {
    this.http.get<any[]>('https://edgalaxy.fankserver.com/overlaps/near/' + systemName).subscribe((v) => {
      this.dataSource.data = v.map((entry) => {
        entry.commodity = `${entry.commodity}${entry.overlapCount}`;
        entry.approvedColor = entry.approved ? 'green' : 'orange';
        return entry;
      });
    });
  }

  toggleDetail() {
    if (this.showDetail) {
      this.displayedColumns = this.basicColumns;
    } else {
      this.displayedColumns = this.detailedColumns;
    }
    this.showDetail = !this.showDetail;
  }

  applyMacro(filter: string) {
    this.filter = filter;
    this.applyFilter(this.filter);
  }
}
