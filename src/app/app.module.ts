import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SelectorComponent } from './journal/selector/selector.component';
import { DetailComponent } from './journal/detail/detail.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatListModule} from '@angular/material/list';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {APP_BASE_HREF, CommonModule, PlatformLocation} from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { DurationPipe } from './duration.pipe';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {FormsModule} from '@angular/forms';
import {OAuthModule} from 'angular-oauth2-oidc';
import {HttpClientModule} from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { StatisticComponent } from './statistic/statistic.component';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import { ExportComponent } from './export/export.component';
import { ImportComponent } from './import/import.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatExpansionModule} from '@angular/material/expansion';
import {MarkdownModule, MarkedOptions} from 'ngx-markdown';
import { ContributeComponent } from './journal/contribute/contribute.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatInputModule} from '@angular/material/input';
import { LogComponent } from './log/log.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatSliderModule} from '@angular/material/slider';
import { LocationsComponent } from './locations/locations.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MaterialStatsComponent } from './statistic/material-stats/material-stats.component';
import { Id64Component } from './id64/id64.component';

@NgModule({
  declarations: [
    AppComponent,
    SelectorComponent,
    DetailComponent,
    DurationPipe,
    StatisticComponent,
    ExportComponent,
    ImportComponent,
    ContributeComponent,
    LogComponent,
    LocationsComponent,
    MaterialStatsComponent,
    Id64Component
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatListModule,
    NgxChartsModule,
    CommonModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatCheckboxModule,
    FormsModule,
    MatSnackBarModule,
    OAuthModule.forRoot(),
    MatProgressBarModule,
    MatIconModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    MatTableModule,
    MatSelectModule,
    MatTooltipModule,
    MatExpansionModule,
    MarkdownModule.forRoot({
      markedOptions: {
        provide: MarkedOptions,
        useValue: {
          breaks: true,
        },
      },
    }),
    MatBottomSheetModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatSliderModule,
    MatAutocompleteModule,
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useFactory: (s: PlatformLocation) => s.getBaseHrefFromDOM(),
      deps: [PlatformLocation]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
