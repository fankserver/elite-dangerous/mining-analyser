import {Component} from '@angular/core';

interface BitClass {
  className: string;
  amount: number;
}

const bitClassesMassA: BitClass[] = [{className: 'mass', amount: 3}, {className: 'zbox', amount: 7}, {className: 'zsector', amount: 6}, {className: 'ybox', amount: 7}, {className: 'ysector', amount: 7}, {className: 'xbox', amount: 7}, {className: 'xsector', amount: 7}, {className: 'n2', amount: 10}, {className: 'n2observed', amount: 1}, {className: 'body', amount: 9}];
const bitClassesMassB: BitClass[] = [{className: 'mass', amount: 3}, {className: 'zbox', amount: 6}, {className: 'zsector', amount: 6}, {className: 'ybox', amount: 6}, {className: 'ysector', amount: 7}, {className: 'xbox', amount: 6}, {className: 'xsector', amount: 7}, {className: 'n2', amount: 12}, {className: 'n2observed', amount: 2}, {className: 'body', amount: 9}];
const bitClassesMassC: BitClass[] = [{className: 'mass', amount: 3}, {className: 'zbox', amount: 5}, {className: 'zsector', amount: 6}, {className: 'ybox', amount: 5}, {className: 'ysector', amount: 7}, {className: 'xbox', amount: 5}, {className: 'xsector', amount: 7}, {className: 'n2', amount: 14}, {className: 'n2observed', amount: 3}, {className: 'body', amount: 9}];
const bitClassesMassD: BitClass[] = [{className: 'mass', amount: 3}, {className: 'zbox', amount: 4}, {className: 'zsector', amount: 6}, {className: 'ybox', amount: 4}, {className: 'ysector', amount: 7}, {className: 'xbox', amount: 4}, {className: 'xsector', amount: 7}, {className: 'n2', amount: 15}, {className: 'n2observed', amount: 5}, {className: 'body', amount: 9}];
const bitClassesMassE: BitClass[] = [{className: 'mass', amount: 3}, {className: 'zbox', amount: 3}, {className: 'zsector', amount: 6}, {className: 'ybox', amount: 3}, {className: 'ysector', amount: 7}, {className: 'xbox', amount: 3}, {className: 'xsector', amount: 7}, {className: 'n2', amount: 14}, {className: 'n2observed', amount: 9}, {className: 'body', amount: 9}];
const bitClassesMassF: BitClass[] = [{className: 'mass', amount: 3}, {className: 'zbox', amount: 2}, {className: 'zsector', amount: 6}, {className: 'ybox', amount: 2}, {className: 'ysector', amount: 7}, {className: 'xbox', amount: 2}, {className: 'xsector', amount: 7}, {className: 'n2', amount: 13}, {className: 'n2observed', amount: 13}, {className: 'body', amount: 9}];
const bitClassesMassG: BitClass[] = [{className: 'mass', amount: 3}, {className: 'zbox', amount: 1}, {className: 'zsector', amount: 6}, {className: 'ybox', amount: 1}, {className: 'ysector', amount: 7}, {className: 'xbox', amount: 1}, {className: 'xsector', amount: 7}, {className: 'n2', amount: 12}, {className: 'n2observed', amount: 17}, {className: 'body', amount: 9}];
const bitClassesMassH: BitClass[] = [{className: 'mass', amount: 3}, {className: 'zsector', amount: 7}, {className: 'ysector', amount: 6}, {className: 'xsector', amount: 7}, {className: 'n2', amount: 10}, {className: 'n2observed', amount: 22}, {className: 'body', amount: 9}];

@Component({
  selector: 'app-id64',
  templateUrl: './id64.component.html',
  styleUrls: ['./id64.component.scss']
})
export class Id64Component {
  public id64 = '';
  public id64Bits: number[] = [];
  public id64BitClasses: string[] = [];
  public id64ClassValues: string[] = [];
  public id64Mass = '';

  constructor() { }

  calculateID64() {
    this.id64Bits.length = 0;
    this.id64BitClasses.length = 0;
    this.id64ClassValues.length = 0;

    let bin = '';
    let rem = 0;
    let i = 1;
    let x = parseInt(this.id64, 10);
    while (x !== 0) {
      rem = x % 2;
      x = parseInt((x / 2).toString(), 10);
      bin += rem.toString();
      i = i * 10;
    }

    const binBits = bin.split('');
    for (let j = 63; j >= 0; j--) {
      let bit = 0;
      if (binBits.length - 1 > j && binBits[j] === '1') {
        bit = 1;
      }
      this.id64Bits.push(bit);
    }

    let classes: BitClass[];
    switch (bin.substring(0, 3).split('').reverse().join('')) {
      case '000':
        classes = bitClassesMassA;
        break;
      case '001':
        classes = bitClassesMassB;
        break;
      case '010':
        classes = bitClassesMassC;
        break;
      case '011':
        classes = bitClassesMassD;
        break;
      case '100':
        classes = bitClassesMassE;
        break;
      case '101':
        classes = bitClassesMassF;
        break;
      case '110':
        classes = bitClassesMassG;
        break;
      case '111':
        classes = bitClassesMassH;
        break;
    }

    let bitIndex = 63;
    for (const c of classes) {
      let values = '';
      for (let j = 0; j < c.amount; j++) {
        this.id64BitClasses.unshift(c.className);

        values = this.id64Bits[bitIndex] + values;
        bitIndex--;
      }
      this.id64ClassValues.push(`${c.className}: ${values}`);
    }
  }
}
