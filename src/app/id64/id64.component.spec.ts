import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Id64Component } from './id64.component';

describe('Id64Component', () => {
  let component: Id64Component;
  let fixture: ComponentFixture<Id64Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Id64Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Id64Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
